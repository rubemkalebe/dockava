FROM ubuntu:18.04

RUN apt-get update

# Install Java.
RUN apt -y install openjdk-8-jdk

# Install Docker
RUN apt -y install curl
RUN curl -fsSL get.docker.com | CHANNEL=stable sh

# Define default command.
CMD ["bash"]
